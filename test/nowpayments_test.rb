require_relative "../lib/nowpayments"
require "minitest/autorun"
require "webmock/minitest"
require "vcr"

#
# NOTE: To re-record new network fixtures, set RECORD=true
# and make sure API_KEY is valid for the MODE you set (sandbox or live)
#
# To print to stdout the network commands, set DEBUG=true
#
DEBUG=false
RECORD=false
API_KEY="1234567-ABCDEFG-HIJKLMN-OPQRSTU"
MODE="sandbox"

class NowpaymentsTest < Minitest::Test

  VCR.configure do |c|
    c.cassette_library_dir = "test/fixtures"
    c.hook_into :webmock
    c.allow_http_connections_when_no_cassette = true
  end

  def setup
    @record_mode = RECORD ? :all : :none
    Nowpayments.configure do |config|
      config.api_key = API_KEY
      config.mode = MODE
      config.debug = DEBUG
    end
  end

  def test_status
    VCR.use_cassette('status', record: @record_mode) do
      r = Nowpayments.status
      assert_equal "OK", r
    end
  end

  def test_currencies
    VCR.use_cassette('currencies', record: @record_mode) do
      currencies = ["1inch", "1inchbsc", "aave", "ada", "algo", "ape", "arpa", "arv", "atom", "ava", "avabsc", "avaerc20", "avax", "avaxc", "avn", "axs", "babydoge", "bat", "bcd", "bch", "bel", "bifi", "blocks", "bnbbsc", "bnbmainnet", "bone", "brise", "bsv", "btc", "btfa", "btg", "bttc", "bttcbsc", "busd", "busdbsc", "c98", "cake", "chr", "chz", "cns", "coti", "cro", "ctsi", "cudos", "cult", "cvc", "dai", "dao", "dash", "dcr", "dgb", "dgmoon", "divi", "doge", "dogecoin", "dot", "egld", "enj", "eos", "etc", "eth", "ethbsc", "ethw", "feg", "fil", "firo", "floki", "flokibsc", "fluf", "front", "ftm", "ftmmainnet", "ftt", "fun", "gafa", "gal", "galaerc20", "gas", "ggtkn", "grt", "gt", "guard", "gusd", "hbar", "hoge", "hot", "hotcross", "ht", "icx", "ilv", "iotx", "keanu", "kiba", "kibabsc", "kishu", "klay", "klv", "kmd", "knc", "leash", "lgcy", "link", "ltc", "luna", "lunc", "mana", "marsh", "matic", "maticmainnet", "mx", "nano", "near", "neo", "nftb", "now", "ntvrk", "nwc", "ocean", "okb", "om", "omg", "one", "ont", "pika", "pit", "poodl", "poolz", "qtum", "quack", "raca", "rbif", "rep", "rvn", "sand", "sfund", "shib", "shibbsc", "sol", "srk", "stpt", "strax", "super", "sxpmainnet", "tenshi", "tfuel", "theta", "tko", "tomo", "trvl", "trx", "ttc", "tusd", "uni", "usdc", "usdcmatic", "usddtrc20", "usdp", "usdt", "usdtbsc", "usdterc20", "usdttrc20", "vet", "vib", "volt", "wabi", "waves", "xcad", "xcur", "xdc", "xem", "xlm", "xmr", "xrp", "xtz", "xvg", "xyo", "yfi", "zbc", "zec", "zen", "zil"]
      response = Nowpayments.currencies
      assert_equal currencies.sort, response.sort
    end
  end

  def test_full_currencies
    VCR.use_cassette('full-currencies', record: @record_mode) do
      currencies = ["1INCH", "1INCHBSC", "AAVE", "ADA", "AE", "ALGO", "APE", "APT", "ARB", "ARK", "ARPA", "ARV", "ATLAS", "ATOM", "AVA", "AVABSC", "AVAERC20", "AVAX", "AVAXC", "AVN", "AXS", "BABYDOGE", "BAD", "BAT", "BCD", "BCH", "BEAM", "BEL", "BIFI", "BLOCKS", "BNBBSC", "BNBMAINNET", "BOBA", "BONE", "BRGBSC", "BRISE", "BRISEMAINNET", "BSV", "BTC", "BTFA", "BTG", "BTTC", "BTTCBSC", "BUSD", "BUSDBSC", "BUSDMATIC", "C98", "CAKE", "CFX", "CHR", "CHZ", "CNS", "COTI", "CRO", "CROMAINNET", "CSPR", "CTSI", "CUDOS", "CULT", "CUSD", "CVC", "DAI", "DAIARB", "DAO", "DASH", "DCR", "DGB", "DGD", "DGMOON", "DINO", "DIVI", "DOGE", "DOGECOIN", "DOT", "EGLD", "ENJ", "EOS", "EPIC", "ETC", "ETH", "ETHARB", "ETHBSC", "ETHW", "EURT", "FDUSDBSC", "FDUSDERC20", "FEG", "FIL", "FIRO", "FITFI", "FLOKI", "FLOKIBSC", "FLUF", "FRONT", "FTM", "FTMMAINNET", "FTT", "FUN", "GAFA", "GAL", "GALAERC20", "GARI", "GAS", "GETH", "GGTKN", "GHC", "GRS", "GRT", "GSPI", "GT", "GUARD", "GUSD", "HBAR", "HEX", "HOGE", "HOT", "HOTCROSS", "HT", "ICX", "ID", "IDBSC", "ILV", "IOTA", "IOTX", "JASMY", "JST", "KAS", "KEANU", "KIBA", "KIBABSC", "KISHU", "KLAY", "KLV", "KLVMAINNET", "KMD", "KNC", "LEASH", "LGCY", "LINK", "LSK", "LTC", "LUNA", "LUNC", "MANA", "MARSH", "MATIC", "MATICMAINNET", "MCO", "MX", "NANO", "NEAR", "NEO", "NFAI", "NFTB", "NOW", "NPXS", "NTVRK", "NWC", "OCEAN", "OKB", "OM", "OMG", "ONE", "ONIGI", "ONT", "PAX", "PIKA", "PIT", "PIVX", "POODL", "POOLX", "POOLZ", "PYUSD", "QTUM", "QUACK", "RACA", "RBIF", "REP", "RJV", "RUNE", "RVN", "RXCG", "SAND", "SFUND", "SHIB", "SHIBBSC", "SNEK", "SOL", "SPI", "SRK", "STKK", "STPT", "STRAX", "SUN", "SUPER", "SXPMAINNET", "SYSEVM", "TENSHI", "TFUEL", "THETA", "TKO", "TLOS", "TLOSERC20", "TOMO", "TON", "TRVL", "TRX", "TTC", "TUP", "TUSD", "TUSDTRC20", "UNI", "USDC", "USDCALGO", "USDCARB", "USDCARC20", "USDCBSC", "USDCKCC", "USDCMATIC", "USDCOP", "USDCSOL", "USDCXLM", "USDDBSC", "USDDTRC20", "USDJ", "USDP", "USDT", "USDTALGO", "USDTARB", "USDTARC20", "USDTBSC", "USDTDOT", "USDTEOS", "USDTERC20", "USDTKAVA", "USDTMATIC", "USDTNEAR", "USDTOP", "USDTSOL", "USDTTRC20", "USDTXTZ", "UST", "VERSE", "VET", "VIB", "VOLT", "WABI", "WAVES", "WBTCMATIC", "WINTRC20", "XAUT", "XCAD", "XCUR", "XDC", "XEC", "XEM", "XLM", "XMR", "XRP", "XTZ", "XVG", "XYM", "XYO", "XZC", "YFI", "ZBC", "ZEC", "ZEN", "ZIL", "ZKSYNC"]
      response = Nowpayments.full_currencies
      response_currency_codes = response.map {|c| c["code"] }
      assert_equal currencies.sort, response_currency_codes.sort
    end
  end

  def test_estimate
    VCR.use_cassette('estimate', record: @record_mode) do
      amount = Nowpayments.estimate(
        currency_from: 'usd',
        amount: 1000,
        currency_to: 'btc'
      )
      assert_equal 0.05886126, amount.to_f
    end
  end

  def test_create_payment
    VCR.use_cassette('payment1', record: @record_mode) do
      payment = Nowpayments.create_payment(
        amount: 1000,
        fiat: "usd",
        coin: "btc"
      )
      assert_equal "4826419410", payment["payment_id"]
      assert_equal "waiting", payment["payment_status"]
      assert_equal 0.05886126, payment["pay_amount"]
    end
  end

  def test_get_payment
    VCR.use_cassette('payment2', record: @record_mode) do
      payment = Nowpayments.get_payment("4826419410")
      assert_equal 4826419410, payment["payment_id"]
      assert_equal "finished", payment["payment_status"]
      assert_equal 0.05886126, payment["pay_amount"]
    end
  end

  def test_wrong_api_key
    VCR.use_cassette('invalid_api_key', record: @record_mode) do
      Nowpayments.configuration.api_key = "bogus"
      exc = assert_raises Nowpayments::Error do
        Nowpayments.full_currencies
      end
      assert_equal "INVALID_API_KEY", exc.code
    end
  end

  def test_wrong_currency
    VCR.use_cassette('payment_wrong_currency', record: @record_mode) do
      exc = assert_raises Nowpayments::Error do
        Nowpayments.create_payment(
          amount: 1000,
          fiat: "usd",
          coin: "blahblah"
        )
      end
      assert_equal "INTERNAL_ERROR", exc.code
    end
  end

  def test_wrong_amount
    VCR.use_cassette('payment_wrong_amount', record: @record_mode) do
      exc = assert_raises Nowpayments::Error do
        Nowpayments.create_payment(
          amount: -1000,
          fiat: "usd",
          coin: "btc"
        )
      end
      assert_equal "INVALID_REQUEST_PARAMS", exc.code
    end
  end

  def test_case_insensitive
    VCR.use_cassette('payment_case_insensitive', record: @record_mode) do
      payment = Nowpayments.create_payment(
        amount: 1000,
        fiat: "usD",
        coin: "bTc"
      )
      assert_equal "waiting", payment["payment_status"]
    end
  end

end
