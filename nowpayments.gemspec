$:.push File.expand_path("lib", __dir__)

require "nowpayments/version"

Gem::Specification.new do |spec|
  spec.name          = "nowpayments"
  spec.version       = Nowpayments::VERSION
  spec.authors       = ["Elijah Waxwing"]
  spec.email         = ["elijah@riseup.net"]

  spec.summary       = %q{Ruby client for the NOWPayments API}
  spec.homepage      = "https://github.com/elijh/nowpayments"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  spec.files = Dir["{lib,test,lib}/**/*", "Rakefile", "README.md", "LICENSE.txt"]

  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "byebug"
  spec.add_development_dependency "minitest"
  spec.add_development_dependency "vcr"
  spec.add_development_dependency "webmock"

  spec.add_dependency "httparty"
end
