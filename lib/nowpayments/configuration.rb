module Nowpayments
  class Configuration
    attr_accessor :version,
      :mode, :sandbox_uri, :production_uri,
      :api_key, :secret_key, :callback_uri, :debug

    def initialize
      @version = 1
      @mode = 'sandbox'
      @sandbox_uri = 'https://api-sandbox.nowpayments.io'
      @production_uri = 'https://api.nowpayments.io'
      @api_key = ''
      @secret_key = ''
      @callback_uri = ''
      @debug = false
    end

    def base_uri
      if @mode == 'sandbox'
        @sandbox_uri
      elsif @mode == 'live'
        @production_uri
      else
        raise ArgumentError, "Nowpayments.configuration.mode is set to '#{@mode}'. Must be 'sandbox' or 'live'."
      end
    end

    def uri
      [base_uri, "v#{@version}"].join('/')
    end
  end
end
