require_relative "nowpayments/version"
require_relative "nowpayments/configuration"
require "httparty"
require "openssl"

#
# Response codes:
# INVALID_API_KEY
# INVALID_REQUEST_PARAMS
# INTERNAL_ERROR
# NOT_FOUND
#
module Nowpayments
  API_KEY_HEADER = "x-api-key"
  SIGNATURE_HEADER = "x-nowpayments-sig"

  class Error < StandardError
    attr_accessor :code
  end

  def self.configure
    yield(configuration) if block_given?
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  private

  def self.endpoint(path)
    [configuration.uri, path].join('/')
  end

  def self.build_error(response)
    message = response['message'] || response.response.msg
    if message =~ /^None of the wallets available/
      message = "The transaction amount is too low"
    end
    exc = Nowpayments::Error.new(message)
    exc.code = response['code'] || "UNKNOWN"
    return exc
  end

  def self.get(cmd, params={})
    url = endpoint(cmd)
    headers = {API_KEY_HEADER => configuration.api_key}
    response = HTTParty.get(url, query: params, headers: headers, debug_output: debug_output)
    if response.success?
      return response
    else
      raise build_error(response)
    end
  end

  def self.post(cmd, params={})
    url = endpoint(cmd)
    headers = {
      API_KEY_HEADER => configuration.api_key,
      "Content-Type" => "application/json"
    }
    response = HTTParty.post(url, body: params.to_json, headers: headers, debug_output: debug_output)
    if response.success?
      return response
    else
      raise build_error(response)
    end
  end

  # sorts a hash by the order of the keys
  def self.sort_hash(h)
    {}.tap do |h2|
      h.sort.each do |k,v|
        h2[k] = v.is_a?(Hash) ? sort_hash(v) : v
      end
    end
  end

  def self.debug_output
    configuration.debug ? $stdout : nil
  end

  public

  def self.status
    response = HTTParty.get(endpoint('status'))
    return response['message']
  end

  def self.currencies
    response = get('currencies')
    return response['currencies']
  end

  def self.full_currencies
    response = get('full-currencies')
    return response['currencies']
  end

  def self.estimate(amount:, currency_from: 'usd', currency_to:)
    response = get('estimate',
      amount: amount,
      currency_from: currency_from,
      currency_to: currency_to
    )
    if response.success?
      response["estimated_amount"]
    else
      nil
    end
  end

  #
  #  amount: amount to charge in fiat currency
  #  fiat: the symbol of the currency for amount
  #  coin: what cryptocurrency the user will pay in
  #  order: arbitrary string to attach to payment record (option)
  #  case: either nil, "fail", or "partially_paid". for testing responses.
  #
  def self.create_payment(amount:nil, fiat:nil, coin:nil, order:nil, test:nil)
    params = {
      "price_amount" => amount,
      "price_currency" => fiat,
      "pay_currency" => coin
    }
    if order
      params["order_id"] = order
    end
    if test
      params["case"] = test
    end
    if configuration.callback_uri && configuration.callback_uri != ""
      params["ipn_callback_url"] = configuration.callback_uri
    end
    response = post('payment', params)
    return response
  end

  def self.get_payment(id)
    get("payment/#{id.to_i}")
  end

  #
  # generate a signature of a response body.
  #
  # This signature can be compared with the SIGNATURE_HEADER
  # value to confirm that the post actually came from Nowpayments.
  #
  def self.sign(body)
    raise StandardError, "configuration.secret_key is not set" unless configuration.secret_key

    # sort the keys in the body, and convert back to a string:
    if body.is_a?(Hash)
      body = sort_hash(body).to_json
    elsif body.is_a?(String)
      body = sort_hash(JSON.parse(body)).to_json
    else
      raise ArgumentError, 'body must be a string or hash'
    end

    # generate a signature on this body
    return OpenSSL::HMAC.hexdigest("SHA512", configuration.secret_key, body)
  rescue StandardError => exc
    raise Nowpayments::Error, exc.to_s
  end
end
